from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def homepage(request):
    return HttpResponse('Hello, Tucurel')


def home(request):
    context = {
        "all_students": [
            {
                'first_name': 'Adelin',
                'last_name': 'Bora',
                'age': 30
            },
            {'first_name': 'Mircea',
             'last_name': 'Popovici',
             'age': 32
             }
        ]
    }
    return render(request, 'home/home.html', context)


def brands(request):
    context = {
        "all_cars": [
            {
                'car_name': 'Audi',
                'model_name': 'E-Tron',
                'engine': 'electic',
                'relase_age': 2021
            },
            {
                'car_name': 'Ford',
                'model_name': 'Kuga',
                'engine': 'disel',
                'relase_age': 2020
            }
         ]
    }
    return render(request, 'home/cars.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'
