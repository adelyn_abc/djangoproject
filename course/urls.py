from django.urls import path

from course import views

urlpatterns = [
    path('teacher_per_courses/<int:teacher_id>/', views.get_teachers_per_course, name='teacher-per-courses')
]
