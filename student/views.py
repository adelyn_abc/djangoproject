from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from student.filters import StudentFilters
from student.forms import StudentForm
from student.models import Student
from teacher.models import Teacher


class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'student/create_student.html'  # calea catre fisierul HTML unde va fi afisat formularul
    model = Student  # modelul = tabelul pe care il folosim pentru generarea formularului
    success_url = reverse_lazy('create-student')  # redirectionarea dupa ce formularul a fost salvat
    form_class = StudentForm
    permission_required = 'student.add_student'

    # def form_valid(self, form):
    #     if form.is_valid and not form.errors:
    #         student = form.save(commit=False)
    #         student.first_name = "Horia"
    #         student.teacher = Teacher.objects.get(id=1)
    #         student.save()
    #         return redirect('list-of-students')000


class StudentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'student/list_students.html'
    model = Student
    context_object_name = 'all_students'
    permission_required = 'student.view_list_of_students'

    def get_context_data(self, **kwargs):
        data = super(StudentListView, self).get_context_data(**kwargs)
        all_students = Student.objects.all()
        my_filter = StudentFilters(self.request.GET, queryset=all_students)
        all_students = my_filter.qs
        data['all_students'] = all_students
        data['my_filter'] = my_filter
        return data
    # def get_queryset(self):
    #     # daca utilizatorul este directorul scolii :
    #     return Student.objects.all().order_by('-first_name')


class StudentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'student/update_student.html'
    model = Student
    success_url = reverse_lazy('list_of_students')
    form_class = StudentForm
    permission_required = 'student.change_student'

    # def get_queryset(self):
    #     return Student.objects.filter(active=1).order_by('-first_name')


class StudentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'student/delete_student.html'
    model = Student
    success_url = reverse_lazy('list-of-students')
    permission_required = 'student.delete_student'


class StudentDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'student/detail_student.html'
    model = Student
    permission_required = 'student.detail_student'
